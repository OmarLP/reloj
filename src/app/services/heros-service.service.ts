import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreModule} from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Animal } from '../interfaces/hero.interface';

@Injectable({
  providedIn: 'root'
})
export class HerosServiceService {
  private heroesCollection: AngularFirestoreCollection<Animal>;

  private heros: Observable<Animal[]>;



  constructor(db: AngularFirestore) {
    this.heroesCollection = db.collection<Animal>('heros');
    this.heros =  this.heroesCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map (a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data};
        });
      }
    ));
  }

  // metodos
  getAllheros() {
    return this.heros;
    }
  getHero() {}
  deleteHero() {}
  updateHeore(heroe: Animal, id: string) {
    return this.heroesCollection.doc(id).update(heroe);

  }
  addHero(hero: Animal) {
    return this.heroesCollection.add(hero);
  }
  getHeroe(id: string) {
    return this.heroesCollection.doc<Animal>(id).valueChanges();
  }

  async revomeHeroe(id: string) {
    return this.heroesCollection.doc(id).delete();
  }


}
