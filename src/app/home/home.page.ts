import { Component, OnInit } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { Animal } from '../interfaces/hero.interface';
import { Routes, Router } from '@angular/router';
import { HerosServiceService } from '../services/heros-service.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  heroes: Animal[] = [];
  constructor(
    private heroServe: HerosServiceService

  ) {}

  ngOnInit(): void {
    this.heroServe.getAllheros().subscribe(res =>  {
      this.heroes = res;
      console.log(this.heroes);
  });
  }
}
