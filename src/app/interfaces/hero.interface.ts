export interface Animal {
    id?: string;
    nombre: string;
    nombre_c: string;
    promedio_v: number;
    alimentacion: string;
    habitad: string;
}